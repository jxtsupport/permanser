!(function($) {
    // regular js
    function formatDate(myDate) {
        var monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var myDay = "<span class='rss-item-pubDate-date'>" + myDate.getUTCDate() + "</span> ";
        var myMonth = "<span class='rss-item-pubDate-month'>" + monthList[myDate.getUTCMonth()] + "</span> ";
        var myYear = "<span class='rss-item-pubDate-full-year'>" + myDate.getUTCFullYear() + "</span> ";

        return myDay + "<br>" + myMonth;
    }

    // jquery
    $(function() {

        

        if ($('#site-topnav .user-loggedIn').length) {
            $('a#HiddenMemLog').prop("href", "/member/default.aspx").text('My Dashboard');
        }

        var currentPage = window.location.pathname.toLowerCase();

        // remove empty li's on the system pages. 
        $("#side-left li:empty").remove();

        // remove empty left side bar
        if ($('#prefix_left-navigation').children().length == 0) {
            $('#prefix_left-navigation').remove();
        }
        if ($('#side-left').children().length == 0) {
            $('#side-left').remove();
        }

        /* Adding Bootstrap Classes */
        // Section > Div.container
        $('#dynamic-container, #content-container, #job-dynamic-container').addClass('container');

        // Content column
        if ($.trim($('#dynamic-side-left-container, #side-left').html()).length) {
            $('#dynamic-content, #content-container #content').addClass('col-sm-8 col-md-9');
            $('#dynamic-side-left-container, #side-left').addClass('col-sm-4 col-md-3');
        } else {
            $('#dynamic-content, #content-container #content').addClass('col-xs-12');
            $('#dynamic-side-left-container, #side-left').addClass("hidden");
        }
        $('#job-dynamic-container #content').addClass('col-xs-12');



        // form elements style
        $('input:not([type=checkbox]):not([type=radio]):not([type=submit]):not([type=reset]):not([type=file]):not([type=image]):not([type=date]), select, textarea').addClass('form-control');
        $('input[type=text]').addClass('form-control');
        $('input[type=submit]').addClass('btn btn-primary');
        $('.mini-new-buttons').addClass('btn btn-primary');
        $('input[type=reset]').addClass('btn btn-default');

        // Responsive table
        $('.dynamic-content-holder table, .content-holder table').addClass('table table-bordered').wrap('<div class="table-responsive"></div>');

        // Convert top menu to Boostrap Responsive menu
        $('.navbar .navbar-collapse > ul').addClass('nav navbar-nav');
        $('.navbar .navbar-collapse > ul > li').has('ul').addClass('dropdown');
        $('.navbar .navbar-collapse > ul > li.dropdown > a').addClass('disabled');
        $('.navbar .navbar-collapse > ul > li.dropdown').append('<a id="child-menu"></a>');
        $('.navbar .navbar-collapse > ul > li.dropdown > a#child-menu').append('<b class="caret"></b>').attr('data-toggle', 'dropdown').addClass('dropdown-toggle');
        $('.navbar .navbar-collapse > ul > li > ul').addClass('dropdown-menu');

        // add placeholder for search widget text field
        $('#keywords1').attr('placeholder', 'Keywords search');

        // add active class to links.
        $("li a[href='" + window.location.pathname.toLowerCase() + "']").parent().addClass("active");
        $("li.active li.active").parent().closest("li.active").removeClass("active");
        $("li li.active").parent().parent().addClass("active")

        // add last-child class to navigation 
        $("#prefix_navigation > ul > li:last").addClass("last-child");

        // add btn style
        $(".backtoresults a").addClass("btn btn-default");
        $(".apply-now-link a").addClass("btn btn-primary");
        $(".button a").addClass("btn btn-default");

        //.left-hidden show
        if ((document.URL.indexOf("/advancedsearch.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }
        if ((document.URL.indexOf("/advancedsearch.aspx?") >= 0)) {
            $(".left-hidden").css("display", "none");
        }
        if ((document.URL.indexOf("/member/createjobalert.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }
        if ((document.URL.indexOf("/member/login.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }
        if ((document.URL.indexOf("/member/register.aspx") >= 0)) {
            $(".left-hidden").css("display", "block");
        }

        // Contact - Google map
        // $("#footer").prepend($("#contact-map"));


        // generate select navigation from sidebar Dynamic menu
        $("#dynamic-content").convertNavigation({
            title: "Related Pages",
            links: "#site-topnav .navbar-nav li.active a:not([data-toggle=dropdown])"
        });

        // generate actions button on Job Listing page
        $(".job-navbtns").convertButtons({
            buttonTitle: "Actions&hellip;",
            title: "Please choose&hellip;",
            links: ".job-navbtns a"
        });

        // generate filters button on Job Listing page
        $(".job-navbtns").convertFilters({
            buttonTitle: "Filters&hellip;",
            filteredTitle: "Applied Filters",
            title: "Please choose&hellip;",
            filtered: ".search-query p",
            list: "ul#side-drop-menu",
            excludeFromList: "#AdvancedSearchFilter_PnlCompany"
        });

        /* System Page Forms */
        if (currentPage == "/member/createjobalert.aspx") {
            setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder1$ucJobAlert1$ddlProfession\',\'\')', 0);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {
                $('.alternate > li > select, #ctl00_ContentPlaceHolder1_ucJobAlert1_txtSalaryLowerBand, #ctl00_ContentPlaceHolder1_ucJobAlert1_txtSalaryUpperBand').addClass('form-control');
                $('#ctl00_ContentPlaceHolder1_ucJobAlert1_ddlProfession, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlRole, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlLocation, #ctl00_ContentPlaceHolder1_ucJobAlert1_lstBoxArea, #ctl00_ContentPlaceHolder1_ucJobAlert1_ddlSalary').addClass('form-control');
            });
        }
        $(document).ajaxComplete(function() {
            $('#divRoleID1 > select, #divAreaDropDown1 > div > select').addClass('form-control');
            $('#divRoleID > select, #divAreaDropDown > div > select').addClass('form-control');
        });
        $('#salaryID').change(function() {
            $(document).ajaxComplete(function() {
                $('#divSalaryFrom > input').addClass('form-control');
                $('#divSalaryTo > input').addClass('form-control');
            });
        });

        function SalaryFromChange1() {
            $(document).ajaxComplete(function() {
                $('#divSalaryTo1 > input').addClass('form-control');
                $('#divSalaryFrom1 > input').addClass('form-control');
            });
        }

        if (currentPage == "/member/register.aspx") {
            $(".uniForm").addClass("border-container");
        }
        if (currentPage == "/member/createjobalert.aspx") {
            $(".uniForm").addClass("border-container");
        }

    });

    // Resize action
    /*$(window).on('resize', function() {

    	var wi = $(this).width();

    	// Mobile & Tablet
    	if ( wi <= 992 ) {
    		//$('#dynamic-side-left-container').before($('#dynamic-content'));
    		//$('#side-left').before($('#content'));    		
    		$('.navbar .navbar-collapse > ul > li.dropdown > a').removeAttr('class');
    	}
    	//  Desktop
    	else {
    		//$('#dynamic-side-left-container').after($('#dynamic-content'));
    		//$('#side-left').after($('#content'));
    		$('.navbar .navbar-collapse > ul > li.dropdown > a').addClass('disabled');
    	} 

    });*/

    $(document).ready(function() {
        
        // forget pswd js
         $('.form-all input[type=\'text\'], .form-all input[type=\'email\'], .form-all input[type=\'password\']').each(function(index, elem) {
                var eId = $(elem).attr('id');
                var span = $(elem).closest('.form-line').children('span')
                if (span.length) {
                    $(elem).attr('placeholder', span.text().replace(/  +/g, ''));
                }
            });

          $('.form-all input[type=\'text\'], .form-all input[type=\'email\'], .form-all input[type=\'password\']').each(function(index, elem) {
                var eId = $(elem).attr('id');
                var span = $(elem).closest('#ef-youremail-field').children('span')
                if (span.length) {
                    $(elem).attr('placeholder', span.text().replace(/  +/g, ''));
                }
            });

         // ellipsis js

        /*// Resize action
        var $window = $(window);
        	// Function to handle changes to style classes based on window width
        	function checkWidth() {
        	if ($window.width() < 992) {
        		$('.navbar .navbar-collapse > ul > li.dropdown > a').removeAttr('class');	
    
        }
        	// Execute on load
        	checkWidth();			
        	// Bind event listener
        	$(window).resize(checkWidth);*/
            if (navigator.platform.toUpperCase().indexOf('MAC') >= 0) {
            jQuery('body').addClass('mac-os');
        }

              //consulant feed : meet the team page
        if( $('#r17_team-member-pages').length ){
            $(".r17_team-member-page").each(function() {
                var dataURL = $(this).attr("data-url");
                $(this).includeFeed({
                    baseSettings: {
                        rssURL: [dataURL || "/ConsultantsRSS.aspx"],
                        limit: 200,
                        addNBSP: false,
                        repeatTag: "consultant"
                    },//end of base setting
                    templates: {
                        itemTemplate: '<div class="row"><div class="staff-holder"><div class="col-sm-4 text-center">'+
                            '<img alt="{{FirstName}} {{LastName}}" src="{{ImageURL}}">'+
                            '<ul class="list-inline" id="site-social">'+
                            '<li><a href="{{LinkedInURL}}" target="_blank"><i class="fa fa-linkedin"><!-- --></i></a></li>'+
                            '<li><a href="{{FacebookURL}}" target="_blank"><i class="fa fa-facebook"><!-- --></i></a></li>'+
                            '<li><a href="{{TwitterURL}}" target="_blank"><i class="fa fa-twitter"><!-- --></i></a></li>'+
                            '</ul>'+
                            '</div>'+
                            '<div class="col-sm-8">'+
                                    '<h2>{{FirstName}} {{LastName}} / <span>{{PositionTitle}}</span></h2><br>{{FullDescription}}'+
                            '</div>'+
                        '</div></div>'
                    },//end of templates
                    complete: function () {
                        
                    }// end of complete function
                }); // end of include feed
            }); // end of team list each

        }

        //testimonial

         $(".testimonial").each(function () {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/newsrss.aspx"]
                    , limit: 200
                , }
                , templates: {
                    itemTemplate: '<div class="testimonial-block"><div class="testimonial-content"><p>{{description}}</p></div><h2>{{title}}</h2></div>'
                }
                , complete: function () {}
            });
        });

        // Home services - carousel
        $('.t-gallery').Gallerycarousel({
            autoRotate: 4000,
            visible: 4,
            speed: 1200,
            easing: 'easeOutExpo',
            itemMinWidth: 250,
            itemMargin: 30
        })


        // Latest Jobs widget
        $("#myJobsList ul").includeFeed({
            baseSettings: {
                rssURL: "/job/rss.aspx?search=1&addlocation=1"
            },
            elements: {
                pubDate: formatDate,
                title: 1,
                description: 1
            },
            complete: function() {
                if ($(this).children().length > 2) {
                    $(this).simplyScroll({
                        frameRate: 60
                    });
                }


         

            }
        });

        //placeholder 
        $("form :input").each(function (index, elem) {
            var eId = $(elem).attr("id");
            var label = null;
            if (eId && (label = $(elem).parents("form").find("label[for=" + eId + "]")).length == 1) {
                $(elem).attr("placeholder", $(label).text().replace(/  +/g, ''));
            }
        });
        $('#salarylowerband').attr('placeholder', 'Minimum Salary');
        $('#salaryupperband').attr('placeholder', 'Minimum Salary');

        // Equal Height	
        $.fn.eqHeights = function(options) {

            var defaults = {
                child: false
            };
            var options = $.extend(defaults, options);
            var el = $(this);
            if (el.length > 0 && !el.data('eqHeights')) {
                $(window).bind('resize.eqHeights', function() {
                    el.eqHeights();
                });
                el.data('eqHeights', true);
            }
            if (options.child && options.child.length > 0) {
                var elmtns = $(options.child, this);
            } else {
                var elmtns = $(this).children();
            }

            var prevTop = 0;
            var max_height = 0;
            var elements = [];
            elmtns.height('auto').each(function() {

                var thisTop = this.offsetTop;
                if (prevTop > 0 && prevTop != thisTop) {
                    $(elements).height(max_height);
                    max_height = $(this).height();
                    elements = [];
                }
                max_height = Math.max(max_height, $(this).height());
                prevTop = this.offsetTop;
                elements.push(this);
            });

            $(elements).height(max_height);
        };

        // Equal Height - Usage
        $('.service-holder').eqHeights();

        // if there is a hash, scroll down to it. Sticky header covers up top of content.
        if ($(window.location.hash).length) {
            $("html, body").animate({
                scrollTop: $(window.location.hash).offset().top - $(".navbar-wrapper").height() - 40
            }, 100);
        }


        // contact page stop scrolling until clicked.
        $(".r27_map-overlay").click(function() {
            $(this).hide();
        });

        //home news
        $("#home-white-paper > div").each(function() {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/NewsRSS.aspx"],
                    limit: 3,
                    addNBSP: false
                },
                templates: {
                    itemTemplate: "<div class='tile item'><div class='block-card'><span class='date'>{{pubDate}}</span><div class='tile-image'><a href'{{link}}'><img src='{{imageurl}}' alt=''></a></div><div class='tile-content'><h3>{{title}}</h3><p>{{description}}</p><p></div></div></div>",
                    pubDateTemplate: "{{pubDate}}"

                },
                predicates: {
                    // example predicate use
                    pubDate: function(pubDate) {
                        var monthList = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                        var dateObj = '';
                        var myDay, myMonth, myYear;

                        //var dateObj = new Date(Date.parse(pubDate));
                        /*if ( !isNaN( dateObj.getTime() )){

                            mnth = monthList[ dateObj.getUTCMonth() ];
                            myDay = "<span class='rss-item-pubDate-date'>" + dateObj.getUTCDate() + "</span> ";
                            myMonth = "<span class='rss-item-pubDate-month'>" + mnth + "</span> ";
                            myYear = "<span class='rss-item-pubDate-full-year'>" + dateObj.getUTCFullYear() + "</span> ";   
                        }*/

                        dateObj = pubDate.split('/');
                        mnth = monthList[parseInt(dateObj[1]) - 1];
                        myDay = "<span class='rss-item-pubDate-date'>" + dateObj[0] + "</span> ";
                        myMonth = "<span class='rss-item-pubDate-month'>" + mnth + "</span> ";
                        myYear = "<span class='rss-item-pubDate-full-year'>" + dateObj[2].substr(0, 4) + "</span> ";


                        return myMonth + myDay + myYear;
                    }
                },
                complete: function() {

                    $('.home-news .owl-carousel').owlCarousel({
                        loop: true,
                        margin: 30,
                        nav: true,
                        responsive: {
                            0: {
                                items: 1
                            },
                            600: {
                                items: 2
                            },
                            1024: {
                                items: 3
                            }
                        }
                    });

                   
                }
            });
        });


        // Home Job
        $("#home-job-paper > div").each(function() {
            var dataURL = $(this).attr("data-url");
            $(this).includeFeed({
                baseSettings: {
                    rssURL: [dataURL || "/job/rss.aspx?search=1&addlocation=1"],
                    
                    addNBSP: false
                },
                templates: {
                    itemTemplate: "<div class='tile item'><div class='block-card'><span class='date'>{{pubDate}}</span></div><div class='tile-content'><h3>{{title}}</h3><p>{{description}}</p><p></div></div></div>",
 pubDateTemplate: "{{pubDate}}"

                },
                predicates: {
                    // example predicate use
                    pubDate: function(pubDate) {
                        var monthList = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
                        var dateObj = '';
                        var myDay, myMonth, myYear;

                        //var dateObj = new Date(Date.parse(pubDate));
                        /*if ( !isNaN( dateObj.getTime() )){

                            mnth = monthList[ dateObj.getUTCMonth() ];
                            myDay = "<span class='rss-item-pubDate-date'>" + dateObj.getUTCDate() + "</span> ";
                            myMonth = "<span class='rss-item-pubDate-month'>" + mnth + "</span> ";
                            myYear = "<span class='rss-item-pubDate-full-year'>" + dateObj.getUTCFullYear() + "</span> ";   
                        }*/

                        dateObj = pubDate.split('/');
                        mnth = monthList[parseInt(dateObj[1]) - 1];
                        myDay = "<span class='rss-item-pubDate-date'>" + dateObj[0] + "</span> ";
                        myMonth = "<span class='rss-item-pubDate-month'>" + mnth + "</span> ";
                        myYear = "<span class='rss-item-pubDate-full-year'>" + dateObj[2].substr(0, 4) + "</span> ";


                        return myDay + myMonth +  myYear;
                    }
                },
                complete: function() {

                    $('.home-jobs .owl-carousel').owlCarousel({
                        loop: true,
                        margin: 30,
                        nav: false,
                        dots: true,
                        responsive: {
                            0: {
                                items: 1
                            },
                            600: {
                                items: 1
                            },
                            1024: {
                                items: 2
                            },
                            1199: {
                                items: 2
                            }
                        }
                    })

                    $(".home-jobs .tile-content p").each(function(i){
            len=$(this).text().length;
            if(len>10)
            {
              $(this).text($(this).text().substr(0,120)+'...');
            }
          });
                }
            });
        });

        $('.dynamic-content-holder h1:first, .dynamic-content-holder .title-inner-page, .content-holder h1, .uniForm border-container h1, .inlineLabels h1').appendTo($('.page-title .container .row div.col-md-12.inner-title'));

        //full width container
        if ($(".full-width").length > 0) {
            $("body").addClass('full-width-container');
        }
        
        if ($(".banner-sec").length > 0) {
            $("body").addClass('home-page');
        }
        
        if ($(".inner-banner").length > 0) {
            $("body").addClass('inner-header');
        }
         
        
    });
    $(window).load(function() {
        $(".banner-sec").height($(window).height());
    });
    $(window).resize(function() {
        $(".banner-sec").height($(window).height());
    });



})(jQuery);